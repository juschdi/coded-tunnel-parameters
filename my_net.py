from mininet.net import Mininet
from mininet.node import OVSKernelSwitch, Host
from mininet.link import TCLink
from mininet.log import info, setLogLevel
from mininet.util import dumpNodeConnections
from subprocess import call
from mininet.cli import CLI
import time

import para_set as ps

ncif = "/home/zzz/project/ncnet/build/bin/ncif"
ncctl = "/home/zzz/project/ncnet/build/bin/ncctl"
protocol = "pacemg"
#tpproto = 17 #UDP
tpproto = 6 #TCP
symbols = 16
symbol_size = 1300
timeout = 0.2
redundancy = 10

loss = 5

class NetTopo():
    def __init__(self):

        self.net = Mininet(host=Host, link=TCLink)

        info('***Add switches***\n')
        s1 = self.net.addSwitch('s1', cls=OVSKernelSwitch)
        s2 = self.net.addSwitch('s2', cls=OVSKernelSwitch)

        info('***Add Hosts***\n')
        h1 = self.net.addHost('h1', cls=Host, ip='10.0.0.1/24')
        h2 = self.net.addHost('h2', cls=Host, ip='10.0.0.2/24')

        info('***Add Link***\n')
        self.net.addLink(s1, s2, bw=100)
        self.net.addLink(s1, h1, bw=100)
        self.net.addLink(s2, h2, bw=100)

        self.net.build()

topo = NetTopo()

def clean_mn(topo):
    call(["mn", "-c"])

def mn_test():
    net = topo.net
    net.start()

    info('***ARP***\n')
    net.switches[0].cmd("""ovs-ofctl add-flow s1 in_port=2,eth_type=0x0806,actions=output:1""")
    net.switches[0].cmd("""ovs-ofctl add-flow s1 in_port=1,eth_type=0x0806,actions=output:2""")
    net.switches[1].cmd("""ovs-ofctl add-flow s2 in_port=1,eth_type=0x0806,actions=output:2""")
    net.switches[1].cmd("""ovs-ofctl add-flow s2 in_port=2,eth_type=0x0806,actions=output:1""")

    info('***ICMP***\n')
    net.switches[0].cmd("""ovs-ofctl add-flow s1 in_port=2,eth_type=0x0800,ip_proto=1,actions=output:1""")
    net.switches[1].cmd("""ovs-ofctl add-flow s2 in_port=1,eth_type=0x0800,ip_proto=1,actions=output:2""")
    net.switches[1].cmd("""ovs-ofctl add-flow s2 in_port=2,eth_type=0x0800,ip_proto=1,actions=output:1""")
    net.switches[0].cmd("""ovs-ofctl add-flow s1 in_port=1,eth_type=0x0800,ip_proto=1,actions=output:2""")

    info('***UDP***\n')
    net.switches[0].cmd("""ovs-ofctl add-flow s1 in_port=2,eth_type=0x0800,ip_proto=17,actions=output:3""")
    net.switches[0].cmd("""ovs-ofctl add-flow s1 in_port=3,eth_type=0x0800,ip_proto=17,actions=output:1""")
    net.switches[1].cmd("""ovs-ofctl add-flow s2 in_port=1,eth_type=0x0800,ip_proto=17,actions=output:3""")
    net.switches[1].cmd("""ovs-ofctl add-flow s2 in_port=3,eth_type=0x0800,ip_proto=17,actions=output:2""")

    info('***TCP***\n')
    net.switches[0].cmd("""ovs-ofctl add-flow s1 in_port=2,eth_type=0x0800,ip_proto=6,tcp_dst=5001,priority=100,actions=output:3""")
    net.switches[0].cmd("""ovs-ofctl add-flow s1 in_port=3,eth_type=0x0800,ip_proto=6,tcp_dst=5001,priority=100,actions=output:1""")
    net.switches[1].cmd("""ovs-ofctl add-flow s2 in_port=1,eth_type=0x0800,ip_proto=6,tcp_dst=5001,priority=100,actions=output:3""")
    net.switches[1].cmd("""ovs-ofctl add-flow s2 in_port=3,eth_type=0x0800,ip_proto=6,tcp_dst=5001,priority=100,actions=output:2""")
    net.switches[1].cmd("""ovs-ofctl add-flow s2 in_port=1,eth_type=0x0800,ip_proto=6,priority=10,actions=output:2""")
    net.switches[0].cmd("""ovs-ofctl add-flow s1 in_port=2,eth_type=0x0800,ip_proto=6,priority=10,actions=output:1""")
    net.switches[1].cmd("""ovs-ofctl add-flow s2 in_port=2,eth_type=0x0800,ip_proto=6,actions=output:1""")
    net.switches[0].cmd("""ovs-ofctl add-flow s1 in_port=1,eth_type=0x0800,ip_proto=6,actions=output:2""")

    assert protocol == "noack" or protocol == "sliding_window" or protocol == "pacemg" or protocol == "nocode"
    print("coding configure\n")
    net.switches[0].cmd("/usr/sbin/sshd -D &")
    net.switches[0].cmd("ifconfig s1 10.0.1.1")
    net.switches[0].cmd( "gnome-terminal -- bash -c " +
        "\"{} --control /run/ncenc.sock -E {} -O symbols={} -O symbol_size={} -O timeout=0.1 -n ncenc -T {} || read\" &".format(
            ncif, protocol, symbols, symbol_size, tpproto))
    net.switches[1].cmd("/usr/sbin/sshd -D &")
    net.switches[1].cmd("ifconfig s2 10.0.2.2")
    net.switches[1].cmd( "gnome-terminal -- bash -c " +
        "\"{} --control /run/ncdec.sock -D {} -O symbols={} -O symbol_size={} -O timeout=0.1 -n ncdec -T {} || read\" &".format(
            ncif, protocol, symbols, symbol_size, tpproto))
    time.sleep(1)
    net.switches[0].cmd("gnome-terminal -- bash -c " +
        "\"{} -C /run/ncenc.sock set redundancy={} || read\" &".format(
            ncctl, redundancy))

    print "Creating network coded tunnel"
    net.hosts[0].cmd('/usr/sbin/sshd -D &')
    net.hosts[1].cmd('/usr/sbin/sshd -D &')
    time.sleep(1)
    net.switches[0].cmd("ip link set dev ncenc up")
    net.switches[0].cmd("ovs-vsctl add-port s1 ncenc")
    net.switches[0].cmd("ovs-ofctl mod-port s1 ncenc no-flood")
    time.sleep(1)
    net.switches[1].cmd("ip link set dev ncdec up")
    net.switches[1].cmd("ovs-vsctl add-port s2 ncdec")
    net.switches[1].cmd("ovs-ofctl mod-port s2 ncdec no-flood")

    #############################

    inf = "s1-eth1"
    r, p = ps.calc_ge(loss, burst=1)
    ps.set_channel(inf, p, r)

    #############################

    if tpproto == 17:
        net.hosts[1].cmd(
            """iperf -s -u -l 1000 -i 1 -y C >> /home/zzz/Note/mn_try_data/iperf_udp_{}_symbols{}_symsize{}_redundancy={}_loss={}.csv &""".format(
                protocol, symbols, symbol_size, redundancy,loss))
        net.hosts[0].cmd("""iperf -c 10.0.0.2 -u -l 1000 -i 1 -t 30 &""")
    elif tpproto == 6:
        net.hosts[1].cmd(
            """iperf -s -i 1 -y C >> /home/zzz/Note/mn_try_data/iperf_tcp_{}_symbols{}_symsize{}_redundancy={}_loss={}.csv &""".format(
                protocol, symbols, symbol_size, redundancy,loss))
        net.hosts[0].cmd("""iperf -c 10.0.0.2 -M 1000 -i 1 -t 30 &""")

    time.sleep(1)
    net.switches[0].cmd("""/home/zzz/project/ncnet/build/bin/ncctl -C /run/ncenc.sock set redundancy={}""".format(redundancy))
    print net.switches[0].cmd("""ovs-vsctl show""")
    print "Dumping host connections"
    dumpNodeConnections(net.hosts)
    dumpNodeConnections(net.switches)
    net.startTerms()
    CLI(net)




    net.stop()
    for switch in net.switches:
        print(switch.cmd("pkill ncif"))
        print(switch.cmd("pkill ncctl"))
    clean_mn(topo)
    call(["rm /run/nc*"], shell=True)
    call(["ifconfig", "ncenc", "down"])
    call(["ifconfig", "ncdec", "down"])


if __name__ == '__main__':
    setLogLevel('info')
    info('*******start******\n')
    mn_test()