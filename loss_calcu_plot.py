import os
import sys
import matplotlib
import numpy as np
from scipy.stats import t
import matplotlib.pyplot as plt
import pandas as pd

protocol = "nocode"
alpha = 0.95
means = []
stds = []
errors = []
conf_intervals = []
dofs = []  # degree of freedom
#redundancies = [10, 20, 40, 100]
redundancies = 40
losses = [5, 10, 15, 20, 25, 30]
for loss in losses:
    data_file = "/home/zzz/Note/mn_try_data/iperf_udp_{}_symbols16_symsize1300_redundancy={}_loss={}.csv".format(
        protocol, redundancies, loss)
    save_dir = "/home/zzz/Note/mn_try_data/iperf_udp_{}_symbols16_symsize1300_redundancy={}_loss={}".format(
        protocol, redundancies, loss)

    iperf_dtype = ['U14', 'U15', int, 'U15', int, int, 'U15', int, int, float, int, int, float, int]
    iperf_dtypenames = ["timestamp", "dst_ip", "dst_port", "src_ip", "src_port", "XXX", "interval", "total_bytes",
                    "goodput", "jitter", "lost_pkts", "total_pkts", "loss_ratio", "out_of_order"]

    iperf_file = open(data_file, "r")
    iperf_file_lines = iperf_file.readlines()
    lines = []

    for line in enumerate(iperf_file_lines):
        data = np.genfromtxt(iperf_file_lines, delimiter=",", names=iperf_dtypenames)
        mean_val = np.mean(data[:-1]["loss_ratio"])
        std = np.std(data[:-1]["loss_ratio"], ddof=1)
        dof = (len(data[:-1]["loss_ratio"])-1)
        error = t.ppf(1-(1-alpha)/2, dof) * std / np.sqrt(dof+1)
        lower_bound = mean_val - error
        upper_bound = mean_val + error
        interval = (lower_bound, upper_bound)

    means.append(mean_val)
    stds.append(std)
    dofs.append(dof)
    errors.append(error)
    conf_intervals.append(interval)

#print means
#print stds
#print dofs
#print errors
#print conf_intervals

x = np.array(losses)
y = means

plt.figure()
plt.plot(x, y)
plt.errorbar(x, y, yerr=errors)
plt.savefig(save_dir)
plt.show()
