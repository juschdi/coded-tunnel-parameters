import os
import sys
import matplotlib as mpl
import numpy as np
from scipy.stats import t
import matplotlib.pyplot as plt
import pandas as pd

protocol = "pacemg"
losses = [5, 10, 15, 20, 25, 30]
redundancies = [0, 10, 20, 40, 100]
iperf_throughput = []

for redundancy in redundancies:
    iperf_throughput = []
    plt.figure()
    for loss in losses:
        data_file = "/home/zzz/Note/mn_try_data/iperf_tcp_{}_symbols16_symsize1300_redundancy={}_loss={}.csv".format(
            protocol, redundancy, loss)
        save_dir = "/home/zzz/Note/mn_try_data/"

        iperf_dtype = ['U14', 'U15', 'uint32', 'U15', 'uint32', 'uint32', 'U15', 'uint32', 'uint32']
        iperf_dtypenames = ["timestamp", "dst_ip", "dst_port", "src_ip", "src_port", "XXX", "interval", "total_bytes",
                        "goodput"]
        iperf_file = open(data_file, "r")
        iperf_file_lines = iperf_file.readlines()
        lines = []

        for line in enumerate(iperf_file_lines):
            data = np.genfromtxt(iperf_file_lines, delimiter=",", names=iperf_dtypenames)
            iperf_throughput = data[:-1]["goodput"]


        x = np.arange(1, len(data), 1)
        y = iperf_throughput
        plt.plot(x,y)
#        colors = plt.rcParams["axes.prop_cycle"].by_key()["color"]   ## blau --> range --> green --> rot --> lila --> braun  ##
#        print colors
    plt.savefig(save_dir + """{}_throughput_redundancy={}""".format(protocol, redundancy))

plt.show()