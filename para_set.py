import configparser
import json
import time
import math
import logging
import os
from fractions import Fraction
from subprocess import call, check_call

nulldev = open("/dev/null", "rw")

def set_channel(inf, p, r, h=0, k=1):
    nulldev = open("/dev/null","rw")
    print("inf:{} p:{} r:{}".format(inf, p, r))
    check_call("echo \"inf:{} p:{} r:{}\" >> /home/zzz/Note/mn_try_data/adapt_nc.log".format(inf, p, r),
               shell=True, stdin=nulldev,stdout=nulldev, stderr=nulldev)
    if r == 1.0:
        pe = p/(1+p)
        check_call("tc qdisc add dev {} parent 5:1 handle 10: netem loss {}%".format(inf, pe * 100),
                   shell=True, stdin=nulldev, stdout=nulldev, stderr=nulldev)
    else:
        check_call("tc qdisc add dev {} parent 1:1 handle 10: netem loss gmodel {}% {}% {}% {}%".format(
            inf, p*100, r*100, (1-h)*100, (1-k)*100
        ), shell=True, stdin=nulldev, stdout=nulldev, stderr=nulldev)

def calc_ge(loss, burst):
    pe = float(loss)/100
    r = 1.0 / float(burst)
    p = float(pe)*r/(1-float(pe))
    return r, p
